#!/bin/bash

echo "CREATE TABLE proxy_list (
	ipID	INTEGER,
	ipNum	VARCHAR(16),
	portNum	INTEGER,
	status	VARCHAR(10),
	PRIMARY KEY (ipID)
);
" > proxy_table.sql

n=1
cat proxy_list | while read x; do
	ipNum=$(echo $x | sed 's/:.*//')
	portNum=$(echo $x | sed 's/.*://')

	echo "Inserting $ipNum:$portNum"

	echo "INSERT INTO proxy_list
	VALUES($n, '$ipNum', '$portNum', 'offline');" >> proxy_values.sql

	n=$((n + 1))
done

sqlite3 proxy_db.db < proxy_table.sql
sqlite3 proxy_db.db < proxy_values.sql
rm -rf *.sql
