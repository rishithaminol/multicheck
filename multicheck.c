#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <sqlite3.h>

#define TOTAL_IP 3145
#define is_online(X) !isonline___(X)

int igniter_threads = 185;
volatile int ip_remain = TOTAL_IP;
pthread_mutex_t lock_ip_remain = PTHREAD_MUTEX_INITIALIZER;
sqlite3 *db;

typedef struct threadFunc_args {
	int id;
} threadFunc_args;

static int callback_isonline(void *data, int argc, char **argv, char **azColName)
{
	char *str;

	str =  (char *)data;
	strcpy(str, argv[0]);

	return 0;
}

/*
	ipID - index number of the ip
	check whether given ID is online
*/
int isonline___(int ipID)
{
	int rc;
	char sql[70] = "";
	char ip_address[15] = "";
	char command[20] = "";
	char *zErrMsg;

	sprintf(sql, "SELECT ipNum FROM proxy_list " \
				"WHERE ipId = %d", ipID);

	rc = sqlite3_exec(db, sql, callback_isonline, (void *)ip_address, &zErrMsg); /* SQLite will call the this callback
												   function for each record processed in each SELECT
												   		statement executed within the SQL argument */
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	sprintf(command, "./isonline %s", ip_address);

	return system(command);
}

static int callback_threadFunc(void *data, int argc, char **argv, char **azColName){
	int i;

	for (i = 0; i < argc; i++)
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");

	printf("\n");

	return 0;
}

void *threadFunc(void *data)
{
	int i, rc;
	char sql[70] = "";
	char *zErrMsg;
	threadFunc_args *arg = (threadFunc_args *)data;

	for (i = arg->id; i <= TOTAL_IP; i += igniter_threads) {
		if (is_online(i))
			sprintf(sql, "UPDATE proxy_list SET status = 'online' WHERE ipID = %d;", i);
		else
			sprintf(sql, "UPDATE proxy_list SET status = 'offline' WHERE ipID = %d;", i);

		pthread_mutex_lock(&lock_ip_remain);
		rc = sqlite3_exec(db, sql, callback_threadFunc, 0, &zErrMsg);
		ip_remain--;
		pthread_mutex_unlock(&lock_ip_remain);

		if (rc != SQLITE_OK) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}

		printf("remain %d\n", ip_remain);
	}

	return NULL;
}

void open_database(void)
{
	if (sqlite3_open("proxy_db.db", &db))
		fprintf(stderr, "Error database open\n");
	else
		fprintf(stderr, "Database openned\n");
}

int main(int argc, char const *argv[])
{
	int i;
	pthread_t thrd_ids[igniter_threads];
	threadFunc_args thrd_args[igniter_threads];

	pthread_mutex_init(&lock_ip_remain, NULL);

	open_database();

	for (i = 0; i < igniter_threads; i++) {
		thrd_args[i].id = i+ 1;
		pthread_create(&thrd_ids[i], NULL, threadFunc, (void *)(thrd_args + i));
	}

	for (i = 0; i < igniter_threads; i++)
		pthread_join(thrd_ids[i], NULL);

	//pthread_mutex_destroy(&lock_ip_remain);

	sqlite3_close(db);
	pthread_exit(NULL);

	return 0;
}
